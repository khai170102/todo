export interface Item {
  name: string;
  success: boolean;
  id: string;
}
