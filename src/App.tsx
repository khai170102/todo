import "antd/dist/antd.css";

import { TodoAdd, TodoList } from "./pages/Todo";

function App() {
  return (
    <div className="py-6 flex justify-center align-middle flex-col ">
      <p className="text-center font-semibold text-[40px]">TODO LIST</p>
      <div className=" w-[50%] self-center">
        <TodoAdd />
      </div>

      <div className=" w-[70%] self-center">
        <TodoList />
      </div>
    </div>
  );
}

export default App;
