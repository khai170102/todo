import { Input, List, message, Modal, Select, Typography } from "antd";
import { useState } from "react";
import { CheckOutlined, DeleteFilled, EditFilled } from "@ant-design/icons";
import {
  useGetPostsQuery,
  useDeletePostMutation,
  useEditPostMutation,
} from "../../redux/_api";
import { Item } from "../../util/types/TypeItem";

export const TodoList = () => {
  const [isModalVisibleEdit, setIsModalVisibleEdit] = useState(false);
  const [isModalVisibleDelete, setIsModalVisibleDelete] = useState(false);
  const [status, setStatus] = useState<string>("All");

  const [newTodoName, setNewTodoName] = useState<string>();
  const [idChose, setIdChose] = useState("");

  const { data: posts } = useGetPostsQuery("");
  const [deletePost] = useDeletePostMutation();
  const [editpost] = useEditPostMutation();

  const handleDelete = (item: string) => {
    let postChose = posts.find((post: Item) => post.name === item);
    setIdChose(postChose.id);
    setIsModalVisibleDelete(true);
  };

  const handleConfirm = () => {
    let postChose = posts.find((post: Item) => post.id === idChose);
    deletePost(postChose.id);
    message.success("Delete Success");
    setIsModalVisibleDelete(false);
  };

  const handleEdit = (item: string) => {
    let postChose = posts.find((post: Item) => post.name === item);
    setIdChose(postChose.id);
    setNewTodoName(item);
    setIsModalVisibleEdit(true);
  };

  const handleSave = () => {
    if (posts.find((post: Item) => post.name === newTodoName)) {
      message.warning("This product already in the list", 0.5);
      return;
    }
    const post = {
      id: idChose,
      name: newTodoName,
      success: false,
    };
    editpost(post);
    message.success("Update Success");
    setIsModalVisibleEdit(false);
  };

  const handleInput = (value: string) => {
    setNewTodoName(value);
  };

  const handleComplete = (item: string) => {
    let postChose = posts.find((post: Item) => post.name === item);

    const post = {
      name: item,
      id: postChose.id,
      success: !postChose.success,
    };
    editpost(post);
  };

  const handleSelect = (value: string) => {
    if (value === "All") {
      setStatus("All");
    } else if (value === "completed") {
      setStatus("completed");
    } else {
      setStatus("uncompleted");
    }
  };

  return (
    <div>
      <div className="flex justify-center p-5">
        <Typography className="mr-4">Fillter : </Typography>
        <Select
          defaultValue="All"
          className="select-before w-32  flex"
          onChange={handleSelect}
        >
          <Select.Option value="All">All</Select.Option>
          <Select.Option value="completed">Completed</Select.Option>
          <Select.Option value="uncompleted">uncompleted</Select.Option>
        </Select>
      </div>
      <List
        itemLayout="horizontal"
        dataSource={posts}
        renderItem={(item: Item) =>
          status === "All" ||
          ("completed" === status ? item.success : !item.success) ? (
            <List.Item
              className={`${item.success ? "bg-green-300" : "bg-slate-200"}`}
              actions={[
                <CheckOutlined
                  onClick={() => handleComplete(item.name)}
                  style={{ fontSize: 16 }}
                />,

                <EditFilled
                  onClick={() => handleEdit(item.name)}
                  style={{ fontSize: 16 }}
                />,
                <DeleteFilled
                  onClick={() => handleDelete(item.name)}
                  style={{ fontSize: 16 }}
                />,
              ]}
            >
              <Typography className="pl-3">{item.name}</Typography>
            </List.Item>
          ) : null
        }
      />

      <Modal
        title=""
        visible={isModalVisibleEdit}
        onOk={handleSave}
        cancelText=""
        okText="Save"
        onCancel={() => setIsModalVisibleEdit(false)}
        closable={false}
        okButtonProps={{ disabled: !(newTodoName && newTodoName.length > 0) }}
      >
        <Input
          placeholder="Input an Update Food Name"
          value={newTodoName}
          onChange={(event) => handleInput(event.currentTarget.value)}
        />
      </Modal>

      <Modal
        title="Confirm"
        visible={isModalVisibleDelete}
        onOk={handleConfirm}
        onCancel={() => setIsModalVisibleDelete(false)}
        okText="Yes"
        cancelText="No"
      >
        <Typography className=" m-auto ">
          You want to delete this ?????
        </Typography>
      </Modal>
    </div>
  );
};
