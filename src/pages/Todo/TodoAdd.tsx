import { Input, Button, message } from "antd";
import { useState } from "react";
import { useAddPostMutation, useGetPostsQuery } from "../../redux/_api";
import { Item } from "../../util/types/TypeItem";

export const TodoAdd = () => {
  const [newTodo, setNewTodo] = useState("");
  const { data: posts } = useGetPostsQuery("");
  const [addPost] = useAddPostMutation();

  const handleSubmit = async () => {
    if (
      posts.find(
        (item: Item) => item.name.toLowerCase() === newTodo.toLowerCase()
      )
    ) {
      message.warning("This product already in the list", 0.5);
      return;
    }

    await addPost({
      name: newTodo,
      success: false,
    });
    message.success("Add Success");
    setNewTodo("");
  };

  return (
    <>
      <div className="flex md:flex-row md:space-x-5 space-x-0 flex-col">
        <Input
          placeholder="Name of the new food"
          value={newTodo}
          onChange={(value) => setNewTodo(value.currentTarget.value)}
        />
        <div className="flex flex-row space-x-3 mt-5 md:mt-0">
          <Button
            type="primary"
            onClick={handleSubmit}
            disabled={!newTodo?.length}
          >
            Add New Todo
          </Button>
        </div>
      </div>
    </>
  );
};
