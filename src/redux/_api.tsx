import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
export const postsApi = createApi({
  reducerPath: "posts",
  baseQuery: fetchBaseQuery({
    baseUrl: import.meta.env.VITE_API_POST,
  }),

  tagTypes: ["Posts"],
  endpoints: (builder) => ({
    getPosts: builder.query({
      query: () => "/posts",
      providesTags: ["Posts"],
    }),

    getPost: builder.query({
      query: (id) => `posts/${id}/`,
    }),

    addPost: builder.mutation({
      query: (post) => ({
        url: "/posts",
        method: "POST",
        body: post,
      }),
      invalidatesTags: ["Posts"],
    }),
    editPost: builder.mutation({
      query(data) {
        const { id, ...body } = data;
        return {
          url: `posts/${id}/`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: ["Posts"],
    }),
    deletePost: builder.mutation({
      query(id) {
        return {
          url: `posts/${id}/`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["Posts"],
    }),
  }),
});

export const {
  useGetPostQuery,
  useGetPostsQuery,
  useAddPostMutation,
  useEditPostMutation,
  useDeletePostMutation,
} = postsApi;
